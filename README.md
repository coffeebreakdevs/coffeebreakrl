# CoffeeBreakRL

Coffee Break RL is a rogue-like/lite game designed to played within a typical coffee break. It is a fantasy RPG with persistant characters and worlds to allow a lasting experience.

## Getting Started

Just grab the latest release from [Itch.io](https://coffeebreakdev.itch.io/coffee-break-rl) to start playing. For those wishing to help with development, please checkout our [GitLab page](https://gitlab.com/coffeebreakdevs/coffeebreakrl).

If you need help playing the game, please consult the [player manual](https://gitlab.com/coffeebreakdevs/coffeebreakrl/wikis/Player-Manual) for details.

If you need help installing the game, please either follow the instructions on Itch.io, or the installation guide below.

## Installation

There are two ways of installing Coffee Break RL:

### Use a pre-built release

The easiest (and recommended) method of installing the game is to use a pre-built release from [Itch.io](https://coffeebreakdev.itch.io/coffee-break-rl). It's free to download and play, and if you want to support us there's a link for that too!

###  Build the game from source

The other way of building the game, which might be applicable if the pre-built version doesn't work for you, is to build it directly from source. To do this you'll need Rust and Cargo installed on your machine. These can both be installed using the [RustUp tool set](https://github.com/rust-lang/rustup.rs/blob/master/README.md). 

After that, you'll need to grab the latest release from [here](https://gitlab.com/coffeebreakdevs/coffeebreakrl/tags). 

Once you've got that, you'll need to compile the Rust source code using Cargo. Simply navigate to the directory where you unzipped the latest version and use the command: `cargo build --release`. This will create an executable and install the game for you! 

## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/coffeebreakdevs/coffeebreakrl/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

You can support us in other ways, for example: 

* Spread the word
* Give us [feedback](https://itch.io/login?return_to=https%3A%2F%2Fcoffeebreakdev.itch.io%2Fcoffee-break-rl&intent=community) (we love hearing from you!)
* Write something on our [wiki](https://gitlab.com/coffeebreakdevs/coffeebreakrl/wikis/home)
* Raise a [bug](https://gitlab.com/coffeebreakdevs/coffeebreakrl/wikis/incoming+coffeebreakdevs-coffeebreakrl-11719213-issue-@incoming.gitlab.com)
* Show us some [love!](https://coffeebreakdev.itch.io/coffee-break-rl/purchase)

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/coffeebreakdevs/coffeebreakrl/tags). 





