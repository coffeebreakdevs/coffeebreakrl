# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.1] - 2019-04-23
### Updated
- CHANGELOG.md
- README.md
- Cargo.toml version number

## [0.1.0] - 2019-04-23
### Added
- Exit system
- Movement system
- Player component
- Exit Component
- Movement Component
- Position Component
- Amethyst GUI with sprites
- Win condition
- Pause state
- Re-bindable keyboard config
- Itch.io CD/CI

## [0.0.0] - 2019-04-07
### Added
- CHANGE LOG (woo!)

