// Use this when assets are added: https://github.com/cs2dsb/github-game-jam-2018/blob/master/src/states/loading.rs
extern crate amethyst;
use amethyst::{
    assets::{Loader, ProgressCounter},
    utils::application_root_dir,
    ecs::prelude::{Entity},
    input::{is_key_down},
    prelude::*,
    renderer::{VirtualKeyCode},
    ui::{Anchor, TtfFormat, UiCreator, UiText, UiTransform},
};

use crate::states::running::RunningState;

#[derive(Default)]
pub struct LoadingState {
    progress: ProgressCounter
}

impl SimpleState for LoadingState {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;
        add_loading_ui(world, &mut self.progress);
    }

    fn handle_event(&mut self, _data: StateData<'_, GameData<'_, '_>>, event: StateEvent) -> Trans<GameData<'static, 'static>, StateEvent> {
        if let StateEvent::Window(event) = &event {
            if is_key_down(&event, VirtualKeyCode::Space) {
                // Switch to the game running state
                return Trans::Switch(Box::new(RunningState::new()))
            }
        }
        // Space isn't pressed, so remain in current state
        Trans::None
    }
}

impl LoadingState {
  fn load_running_prefab(&mut self, world: &mut World) {
    let loader = world.read_resource::<Loader>();
  }
}

fn add_loading_ui(world: &mut World, progress: &mut ProgressCounter) {
    // Add loading UI to screen
    let ui_path = format!("{}/resources/ui/loading.ron", application_root_dir());
    world.exec(|mut creator: UiCreator| {
        creator.create(ui_path, progress);
    });
}