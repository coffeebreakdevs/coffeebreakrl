extern crate amethyst;
use amethyst::{
    assets::{Loader},
    ecs::prelude::{Entity},
    input::{is_key_down},
    prelude::*,
    renderer::{VirtualKeyCode},
    ui::{Anchor, TtfFormat, UiText, UiTransform},
};

pub struct WinState;

impl SimpleState for WinState {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;
        initialise_titletext(world);
    }

    fn handle_event(&mut self, _data: StateData<'_, GameData<'_, '_>>, event: StateEvent) -> Trans<GameData<'static, 'static>, StateEvent> {
        if let StateEvent::Window(event) = &event {
            if is_key_down(&event, VirtualKeyCode::Escape) {
                // Unpause game by popping the PausedState
                return Trans::Quit;
            }
        }
        // Escape isn't pressed, so remain in current state
        Trans::None
    }
}

/// TitleText contains the ui text components that display the title
pub struct TitleText {
    pub title: Entity,
}

fn initialise_titletext(world: &mut World) {
    let font = world.read_resource::<Loader>().load(
        "resources/font/square.ttf",
        TtfFormat,
        Default::default(),
        (),
        &world.read_resource(),
    );
    let transform = UiTransform::new(
        "P1".to_string(), Anchor::TopMiddle,
        0.0, -50.0, 1.0, 200.0, 50.0, 0,
    );

    let title = world
        .create_entity()
        .with(transform)
        .with(UiText::new(
            font.clone(),
            "YOU WIN!".to_string(),
            [1., 1., 1., 1.],
            50.,
        )).build();

    world.add_resource(TitleText { title });
}
