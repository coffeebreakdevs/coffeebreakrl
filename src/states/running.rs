extern crate amethyst;
use amethyst::{
    assets::{AssetStorage, Loader},
    input::{is_key_down},
    prelude::*,
    ecs::prelude::{Entity},
    renderer::{VirtualKeyCode, Camera, PngFormat, Projection, SpriteRender, SpriteSheet, SpriteSheetFormat, SpriteSheetHandle, Texture, TextureMetadata, PosNormTex},
    ui::{Anchor, TtfFormat, UiText, UiTransform},
    utils::scene::{BasicScenePrefab}
};
use amethyst::core::Transform;

use crate::components::Position;
use crate::components::Velocity;
use crate::components::Exit;
use crate::components::Player;
use crate::components::Collidable;

pub const ARENA_HEIGHT: f32 = 256.0;
pub const ARENA_WIDTH: f32 = 256.0;

pub struct RunningState;

impl SimpleState for RunningState {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;

        // Load the spritesheet necessary to render the graphics.
        // `spritesheet` is the layout of the sprites on the image;
        // `texture` is the pixel data.
        
        // This should be handled by a renderer
        let sprite_sheet_handle = load_sprite_sheet(world);

        //world.register::<Exit>();

        initialise_exit(world, sprite_sheet_handle.clone());
        initialise_player(world, sprite_sheet_handle);
        initialise_scoreboard(world);
        initialise_camera(world);
    }

    // Move this command system
    // fn handle_event(&mut self, _data: StateData<'_, GameData<'_, '_>>, event: StateEvent) -> Trans<GameData<'static, 'static>, StateEvent> {
    //     if let StateEvent::Window(event) = &event {
    //         if is_key_down(&event, VirtualKeyCode::Escape) {
    //             use crate::game_core::pause::PausedState;
    //             // Pause game by transiting to PausedState
    //             return Trans::Push(Box::new(PausedState));
    //         }
    //     }
    //     // Escape isn't pressed, so remain in current state
    //     Trans::None
    // }
}

fn load_sprite_sheet(world: &mut World) -> SpriteSheetHandle {
    // Load the sprite sheet necessary to render the graphics.
    // The texture is the pixel data
    // `sprite_sheet` is the layout of the sprites on the image
    // `texture_handle` is a cloneable reference to the texture
    let texture_handle = {
        let loader = world.read_resource::<Loader>();
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        loader.load(
            "resources/texture/spritesheet.png",
            PngFormat,
            TextureMetadata::srgb_scale(),
            (),
            &texture_storage,
        )
    };

    let loader = world.read_resource::<Loader>();
    let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
    loader.load(
    "resources/texture/spritesheet.ron", // Here we load the associated ron file
    SpriteSheetFormat,
    texture_handle, // We pass it the handle of the texture we want it to use
    (),
    &sprite_sheet_store,
)
}


/// Initialise the camera.
fn initialise_camera(world: &mut World) {
    let mut transform = Transform::default();
    transform.set_z(1.0);;

    world
        .create_entity()
        .with(Camera::from(Projection::orthographic(
            0.0,
            ARENA_WIDTH,
            0.0,
            ARENA_HEIGHT,
        )))
        .with(transform)
        .build();
}

/// Initialises player
fn initialise_player(world: &mut World, sprite_sheet_handle: SpriteSheetHandle) {
    let pos_x: i32 = 16;
    let pos_y: i32 = 16;
    // This should be handled by a renderer at some point
    let mut transform = Transform::default();
    transform.set_x(pos_x as f32);
    transform.set_y(pos_y as f32);

    // Assign the sprites for the player
    let sprite_render = SpriteRender {
        sprite_sheet: sprite_sheet_handle.clone(),
        sprite_number: 1, // Player is the second sprite in the sprite_sheet
    };

    // Create a player entity.
    world
        .create_entity()
        .with(sprite_render.clone())
        .with(Position::new(pos_x, pos_y))
        .with(Velocity::new())
        .with(transform)
        .with(Player::new())
        .with(Collidable::new())
        .build();
}

/// Initialises exit entity
fn initialise_exit(world: &mut World, sprite_sheet_handle: SpriteSheetHandle) {
    let pos_x: i32 = 128;
    let pos_y: i32 = 128;
    // This should be handled by a renderer at some point
    let mut transform = Transform::default();
    transform.set_x(pos_x as f32);
    transform.set_y(pos_y as f32);

    // Assign the sprites for the exit
    let sprite_render = SpriteRender {
        sprite_sheet: sprite_sheet_handle.clone(),
        sprite_number: 9, // exit is the eighth sprite in the sprite_sheet
    };

    // Create a exit entity.
    world
        .create_entity()
        .with(sprite_render.clone())
        .with(Position::new(pos_x, pos_y))
        .with(transform)
        .with(Exit::new())
        .with(Collidable::new())
        .build();
}

pub struct ScoreText {
    pub p1_score: Entity,
}

fn initialise_scoreboard(world: &mut World) {
    let font = world.read_resource::<Loader>().load(
        "resources/font/square.ttf",
        TtfFormat,
        Default::default(),
        (),
        &world.read_resource(),
    );
    let p1_transform = UiTransform::new(
        "P1".to_string(), Anchor::TopMiddle,
        0.0, -50.0, 1.0, 200.0, 50.0, 0,
    );

    let p1_score = world
        .create_entity()
        .with(p1_transform)
        .with(UiText::new(
            font.clone(),
            " ".to_string(),
            [1., 1., 1., 1.],
            50.,
        )).build();

    world.add_resource(ScoreText { p1_score });
}

impl RunningState {
    pub fn new() -> Self {
         Self {}
    }
}

pub type RunningPrefabData = BasicScenePrefab<Vec<PosNormTex>>;