use std::fmt;
use amethyst::{
    ecs::prelude::{Component, VecStorage},
};

// Position Component
#[derive(Debug, Clone)]
pub struct Position {
  pub x: i32,
  pub y: i32,
}

impl Position {
    pub fn new(x: i32, y: i32) -> Position {
        Position { x, y }
    }
}

impl Component for Position {
    type Storage = VecStorage<Self>;
}

// Velocity Component
#[derive(Debug, Clone)]
pub struct Velocity {
  pub x: i32,
  pub y: i32,
}

impl Velocity {
    pub fn new() -> Velocity {
        Velocity { x: 0, y: 0 }
    }
}

impl Component for Velocity {
    type Storage = VecStorage<Self>;
}

// Collidable Component
#[derive(Debug, Clone)]
pub struct Collidable {}

impl Collidable {
    pub fn new() -> Collidable {
        Collidable {}
    }
}

impl Component for Collidable {
    type Storage = VecStorage<Self>;
}


// Exit Component
#[derive(Debug, Clone)]
pub struct Exit {}

impl Exit {
    pub fn new() -> Exit {
        Exit {}
    }
}

impl Component for Exit {
    type Storage = VecStorage<Self>;
}

// Player Component
#[derive(Debug, Clone)]
pub struct Player {}

impl Player {
    pub fn new() -> Player {
        Player {}
    }
}

impl Component for Player {
    type Storage = VecStorage<Self>;
}