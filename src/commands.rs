use amethyst::shrev::EventChannel;

///Commands that various systems listen for. Most are user input.
#[derive(Debug)]
pub enum Command {
  MoveUp,
  MoveDown,
  MoveLeft,
  MoveRight,
  PauseMenu,
}

///This is the channel resource commands get sent to
pub type CommandChannel = EventChannel<Command>;