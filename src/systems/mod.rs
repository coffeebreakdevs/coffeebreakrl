use amethyst::{
  GameDataBuilder,
  core::transform::TransformBundle,
  Error
};

pub mod input_system;
pub mod velocity_system;
pub mod exit_system;
pub mod movement_system;
pub mod pause_system;
pub mod collision_system;

pub fn register_systems<'a, 'b>(builder: GameDataBuilder<'a, 'b>) -> GameDataBuilder<'a, 'b> {
  builder
    .with(input_system::InputSystem::default(), "player_input_system", &["input_system"])
    .with(pause_system::PauseSystem::default(), "pause_menu_system", &[])
    .with(movement_system::MovementSystem::default(), "new_movement_system", &[])
    .with_barrier()
    .with(collision_system::CollisionSystem::default(), "new_collision_system", &[])
    .with(velocity_system::VelocitySystem::default(), "new_velolcity_system", &["new_collision_system"])
    .with(exit_system::ExitSystem::default(), "new_level_system", &[])
}