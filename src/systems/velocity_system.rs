extern crate amethyst;

use amethyst::core::Transform;
use amethyst::ecs::{Join, ReadStorage, System, WriteStorage};

use crate::components::Position;
use crate::components::Velocity;

#[derive(Default)]
pub struct VelocitySystem {}

impl<'s> System<'s> for VelocitySystem {
  type SystemData = (
    WriteStorage<'s, Position>,
    WriteStorage<'s, Velocity>,
    WriteStorage<'s, Transform>,
  );

  fn run(&mut self, (mut positions, mut velocities, mut transforms): Self::SystemData) {
    for (position, velocity, transform) in (&mut positions, &mut velocities, &mut transforms).join() {
          position.x += velocity.x;
          position.y += velocity.y;
          
          // Move to renderer
          transform.translate_y(velocity.y as f32);
          transform.translate_x(velocity.x as f32);

          // reset velocity
          velocity.y = 0;
          velocity.x = 0;
      }
  }
}



