extern crate amethyst;

use amethyst::core::Transform;
use amethyst::ecs::{Join, ReadStorage, System, WriteStorage};

use crate::components::Position;
use crate::components::Velocity;
use crate::components::Collidable;

#[derive(Default)]
pub struct CollisionSystem {}

impl<'s> System<'s> for CollisionSystem {
  type SystemData = (
    ReadStorage<'s, Position>,
    WriteStorage<'s, Velocity>,
    ReadStorage<'s, Collidable>,
  );

  fn run(&mut self, (positions, mut velocities, collidables): Self::SystemData) {
    for (position, velocity, collidable) in (&positions, &mut velocities, &collidables).join() {
          let temp_position_x = position.x + velocity.x;
          let temp_position_y = position.y + velocity.y;
          for (collidable_position, _collidable) in (&positions, &collidables).join() {
                if temp_position_x == collidable_position.x && temp_position_y == collidable_position.y {
                        velocity.y = 0;
                        velocity.x = 0;
                }
            }
        }
    }
}
