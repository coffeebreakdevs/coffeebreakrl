use std::collections::HashSet;

extern crate amethyst;

use amethyst::ecs::{Read, System, Write};
use amethyst::input::InputHandler;

use crate::commands::{Command, CommandChannel};

#[derive(Default)]
pub struct InputSystem {
  down_actions: HashSet<String>,
}

impl<'s> System<'s> for InputSystem {
  type SystemData = (
    Read<'s, InputHandler<String, String>>,
    Write<'s, CommandChannel>,
  );

  fn run(&mut self, (input, mut commands): Self::SystemData) {
      for action in input.bindings.actions() {
      let was_down = self.down_actions.contains(&action);
      let is_down = input.action_is_down(&action).unwrap_or(false);

      let pressed = !was_down && is_down;
      let released = was_down && !is_down;

      if released {
        self.down_actions.remove(&action);
      } else if pressed {
        let cmd = match action.as_ref() {
          "MoveUp" => Some(Command::MoveUp),
          "MoveDown" => Some(Command::MoveDown),
          "MoveLeft" => Some(Command::MoveLeft),
          "MoveRight" => Some(Command::MoveRight),
          "PauseMenu" => Some(Command::PauseMenu),
          _o => {
            //debug!("Unhandled input action: {:?}", o);
            None
          },
        };
        if let Some(cmd) = cmd {
          commands.single_write(cmd);
        }
        self.down_actions.insert(action);
      }
    }
  }
}
    //     use std::{thread, time};
    //     let delay = time::Duration::from_millis(100);
    //     thread::sleep(delay);
    // }
    // move to velocity system/component
    // Move to command architecture
//         let mut movement_x: f32 = 0.0;
//         let mut movement_y: f32 = 0.0;
//         if action == "MoveUp"{
//           movement_y = 16.0;
//         };
//         if action == "MoveDown"{
//           movement_y = -16.0;
//         };
//         if action == "MoveLeft"{
//           movement_x = -16.0;
//         };
//         if action == "MoveRight"{
//           movement_x = 16.0;
//         };
//     for (velocity, _transform) in (&mut velocities, &mut transforms).join() {
//         velocity.y = movement_y as i32;
//         velocity.x = movement_x as i32;
//       }
//     }
//   }
// }


