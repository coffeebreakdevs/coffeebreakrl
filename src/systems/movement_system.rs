extern crate amethyst;

use amethyst::ecs::{Join, Read, System, WriteStorage};
use amethyst::ecs::prelude::*;
use amethyst::shrev::ReaderId;

use crate::components::Velocity;
use crate::commands::{Command, CommandChannel};

#[derive(Default)]
pub struct MovementSystem {
    command_reader: Option<ReaderId<Command>>,
}

impl<'s> System<'s> for MovementSystem {
    type SystemData = (
        Read<'s, CommandChannel>,
        WriteStorage<'s, Velocity>,
  );

    fn setup(&mut self, res: &mut Resources) {
    Self::SystemData::setup(res);
    self.command_reader = Some(res.fetch_mut::<CommandChannel>().register_reader());
    }

    fn run(&mut self, (commands, mut velocities): Self::SystemData) {
        let mut move_up = false;
        let mut move_down = false;
        let mut move_left = false;
        let mut move_right = false;
        for command in commands.read(self.command_reader.as_mut().unwrap()) {
            match command {
                Command::MoveUp => move_up = true,
                Command::MoveDown => move_down = true,
                Command::MoveLeft => move_left = true,
                Command::MoveRight => move_right = true,
                _ => {},
            }
        }
        if move_up {
            for velocity in (&mut velocities).join() {
            velocity.y = 16;
            }
        }
        if move_down {
            for velocity in (&mut velocities).join() {
            velocity.y = -16;
            }
        }
        if move_left {
            for velocity in (&mut velocities).join() {
            velocity.x = -16;
            }
        }
        if move_right {
            for velocity in (&mut velocities).join() {
            velocity.x = 16;
            }
        }
    }
}


