extern crate amethyst;

use amethyst::Trans;
use amethyst::ecs::{Join, Read, System, WriteStorage};
use amethyst::ecs::prelude::*;
use amethyst::shrev::ReaderId;

use crate::states::paused::PausedState;
use crate::commands::{Command, CommandChannel};
// Remove after testing
use crate::components::Velocity;

#[derive(Default)]
pub struct PauseSystem {
    command_reader: Option<ReaderId<Command>>,
}

impl<'s> System<'s> for PauseSystem {
    type SystemData = (
        Read<'s, CommandChannel>,
        WriteStorage<'s, Velocity>,
  );

    fn setup(&mut self, res: &mut Resources) {
    Self::SystemData::setup(res);
    self.command_reader = Some(res.fetch_mut::<CommandChannel>().register_reader());
    }

    fn run(&mut self, (commands, velocities): Self::SystemData) {
        let mut pause = false;
        for command in commands.read(self.command_reader.as_mut().unwrap()) {
            match command {
                Command::PauseMenu => pause = true,
                _ => {},
            }
        }
        if pause {
            println!("Pause Key Pressed!");
            Trans::Push(Box::new(PausedState));
        }
    }
}


