extern crate amethyst;

use amethyst::ecs::{Join, ReadStorage, ReadExpect, System,  WriteStorage};
use amethyst::Trans;
use amethyst::ecs::prelude::*;

use crate::components::Position;
use crate::components::Exit;
use crate::components::Player;
use crate::states::win::WinState;

#[derive(Default)]
pub struct ExitSystem {}

impl<'s> System<'s> for ExitSystem {
  type SystemData = (
    ReadStorage<'s, Position>,
    ReadStorage<'s, Exit>,
    ReadStorage<'s, Player>,
  );

  fn run(&mut self, (positions, exits, players): Self::SystemData) {
    for (player_position, _player) in (&positions, &players).join() {
        println!("Player: {} {}", player_position.x, player_position.y);
        for (exit_position, _exit) in (&positions, &exits).join() {
            println!("Exit: {} {}", exit_position.x, exit_position.y);
            if player_position.x == exit_position.x && player_position.y == exit_position.y {
                // Replace with new level transition and create a separate win system
                    println!("Game won!");
                    Trans::Push(Box::new(WinState));
                }
            }
        }
    }
}