extern crate amethyst;
use amethyst::{
    core::transform::TransformBundle,
    prelude::*,
    renderer::{DisplayConfig, DrawFlat2D, Pipeline, RenderBundle, Stage},
    utils::application_root_dir,
    ui::{DrawUi, UiBundle},
    assets::PrefabLoaderSystem,
    input::InputBundle
};

mod states;
use states::{RunningPrefabData,};

mod systems;
use systems::register_systems;

mod components;

mod commands;

// mod rendering;
// use rendering::configure_rendering;

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    // Initialise inputs
    let binding_path = format!("{}/resources/bindings_config.ron", application_root_dir());
    let input_bundle = InputBundle::<String, String>::new()
        .with_bindings_from_file(binding_path)?;

    let path = format!("{}/resources/display_config.ron", application_root_dir());
    let display_config = DisplayConfig::load(&path);

    let pipe = Pipeline::build().with_stage(
        Stage::with_backbuffer()
            .clear_target([0.0, 0.0, 0.0, 1.0], 1.0)
            .with_pass(DrawFlat2D::new())
            .with_pass(DrawUi::new()),
    );
    let mut game_data = GameDataBuilder::default()
        .with_bundle(RenderBundle::new(pipe, Some(display_config)).with_sprite_sheet_processor())?
        .with_bundle(TransformBundle::new())?
        .with_bundle(input_bundle)?
        .with_bundle(UiBundle::<String, String>::new())?;
    
    game_data = register_systems(game_data);

    let mut game = Application::new("./", states::loading::LoadingState::default(), game_data)?;
    game.run();
    Ok(())
}

